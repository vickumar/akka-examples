name := "akka-examples"

version := "0.0.1"

organization := "org.vickumar"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.12",
  "com.typesafe.akka" %% "akka-remote" % "2.5.12",
  "com.rabbitmq" % "amqp-client" % "3.5.7"
)

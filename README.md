### Google Slides

- https://docs.google.com/presentation/d/1UDpDXJ2Hl1fgJJAZ5PqUaNVBKauTWUZbgKUI587Wz-E/edit?usp=sharing

### Run the Ping-Pong example

(note: please install sbt and scala)

- sbt "run-main PingPong"

### Run the Retrying actor example

- sbt "run-main Retry"

### Run the Retry with Supervision example

- sbt "run-main RetrySupervisor"

### Run the Remote actor example

- sbt "run-main RemoteActor"

### Run the AMQP/RabbitMq example

- sbt "run-main AmqpExample"
import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor._

import scala.concurrent.duration._

class RetrySupervisor extends Actor {
  def failedOp = {
    Thread.sleep(2000)
    throw new Exception("Actor failed !!!")
  }

  override def preRestart(reason: Throwable, message: Option[Any]) = {
    println("\nI am restarting...\n")
    super.preRestart(reason, message)
  }

  override def postRestart(reason: Throwable) = {
    println("\n...restart completed!\n")
    self ! StartMessage
    super.postRestart(reason)
  }

  override def preStart() = println("\nI am alive!\n")
  override def postStop() = println("\nGoodbye world!\n")

  override val supervisorStrategy: SupervisorStrategy = {
    OneForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 10 seconds) {
      case _: Exception => Restart
    }
  }
  def receive = {
    case StartMessage =>
      print(s"\n\n${self.path}\n\n")
      print(s"Trying operation")
      failedOp
  }
}

object RetrySupervisor extends App {
  val system = ActorSystem("RetryActorSystem")
  val retry = system.actorOf(Props[RetrySupervisor], name = "retry-actor")
  retry ! StartMessage
}
import akka.actor._
import akka.remote.RemoteScope

class RemoteActor extends Actor {

  def receive = {
    case s: String =>
      print(s"\n\n${self.path}\n\n")
      print(s"\n\nI received this message: $s\n\n")
  }
}

object RemoteActor extends App {
  val system = ActorSystem("remoteActorSystem")
  //val remoteActor = system.actorOf(Props[RemoteActor], "remoteActor")

  val one = AddressFromURIString("akka.tcp://remoteActorSystem@127.0.0.1:2552")
  val two = Address("akka.tcp", "remoteActorSystem", "127.0.0.1", 2552)

  val remoteRef = system.actorOf(Props[RemoteActor].
    withDeploy(Deploy(scope = RemoteScope(two))), "remoteActor")
  remoteRef ! "This is a test message"
}
import akka.actor._
import akka.util.Timeout
import com.rabbitmq.client.{QueueingConsumer, Channel, Connection, ConnectionFactory}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext


object AmqpSender {
  private val connection: Connection = getConnection()
  private val exchange: String = "test-exchange"

  lazy val processor = ActorSystem("syncClient")

  implicit val ec = ExecutionContext.Implicits.global
  implicit lazy val timeout = Timeout(60 seconds)

  def getConnection(): Connection = {
    val factory = new ConnectionFactory()
    factory.setHost("localhost")
    factory.setVirtualHost("/")
    factory.setPort(5672)
    factory.setUsername("guest")
    factory.setPassword("guest")
    factory.newConnection()
  }

  def getSender() = {
    val senderChannel= connection.createChannel()
    val sender = processor.actorOf(Props(
      new AmqpPublisher(channel = senderChannel, exchange = exchange)))
    sender
  }
}

class AmqpPublisher(channel: Channel, exchange: String) extends Actor {
  def receive = {
    case msg: String => {
      channel.basicPublish(exchange, "", null, msg.getBytes())
      print(s"\nPublished: $msg\n")
      sender ! true
    }
    case _ => {}
  }
}
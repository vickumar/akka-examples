import akka.actor._

import scala.util.{Failure}

import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits.global

case class TryOperation(numFailures: Int = 1, maxFailures: Int = 5)

class Retry extends Actor {
  def failedOp = {
    Thread.sleep(1000)
    Failure(new Throwable("Operation has failed"))
  }

  def receive = {
    case t: TryOperation =>
      print(s"\n\n${self.path}\n\n")
      print(s"Trying operation, failures ${t.numFailures}")
      failedOp match {
        case Failure(_) => {
          if (t.numFailures < t.maxFailures) {
            context.system.scheduler.scheduleOnce(1 second, self, TryOperation(t.numFailures + 1))
          }
          else {
            print("\n\nMax failures reached...\n")
            context.stop(self)
          }
        }
        case _ => print("Operation was successful")
      }
  }
}

object Retry extends App {
  val system = ActorSystem("RetryActorSystem")
  val retry = system.actorOf(Props[Retry], name = "retry-actor")
  retry ! TryOperation()
}
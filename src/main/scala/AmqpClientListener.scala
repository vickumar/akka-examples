import akka.actor.{Actor, ActorSystem, actorRef2Scala, Props => AkkaProps}
import akka.util.Timeout
import com.rabbitmq.client.{Channel, Connection, ConnectionFactory, QueueingConsumer}

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext


case class StartListening()
case class SendClientMessage(id: String, msg: String)

object AmqpClientListener {
  private val connection: Connection = getConnection()
  private val exchange: String = "test-exchange"
  lazy val processor = ActorSystem("syncServer")
  implicit val ec = ExecutionContext.Implicits.global
  implicit lazy val timeout = Timeout(60 seconds)

  private def getConnection(): Connection = {
    val factory = new ConnectionFactory()
    factory.setHost("localhost")
    factory.setVirtualHost("/")
    factory.setPort(5672)
    factory.setUsername("guest")
    factory.setPassword("guest")
    factory.newConnection()
  }

  def processCallback(msg: String) = {
    print("\nRecieved message '%s'\n".format(msg))
  }

  def startListening() = {
    print("\nStarted listening\n")
    val listenChannel = connection.createChannel()
    listenChannel.exchangeDeclare(exchange, "fanout")
    setupListener(listenChannel,listenChannel.queueDeclare().getQueue(), exchange, processCallback)
  }

  private def setupListener(channel: Channel, queueName: String, f: (String) => Any) {
    val listen = processor.actorOf(AkkaProps(new ListeningClient(channel, queueName, f)))
    processor.scheduler.scheduleOnce(1 second, listen, StartListening)
  }

  private def setupListener(channel: Channel, queueName : String, exchange: String, f: (String) => Any) {
    channel.queueBind(queueName, exchange, "")
    setupListener(channel, queueName, f)
  }
}

class ListeningClient(channel: Channel, queue: String, f: (String) => Any) extends Actor {

  def receive = {
    case _ => startReceving
  }

  def startReceving = {

    val consumer = new QueueingConsumer(channel)
    channel.basicConsume(queue, true, consumer)

    while (true) {
      val delivery = consumer.nextDelivery()
      val msg = new String(delivery.getBody())

      context.actorOf(AkkaProps(new Actor {
        def receive = {
          case some: String => f(some)
        }
      })) ! msg
    }
  }
}
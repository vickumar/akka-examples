
object AmqpExample extends App {
  AmqpClientListener.startListening()
  val amqpSender = AmqpSender.getSender()
  amqpSender ! "Test sending a message"
}
import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{ActorSystem, AllForOneStrategy, OneForOneStrategy, Props, SupervisorStrategy}
import akka.pattern.{Backoff, BackoffSupervisor}

import scala.concurrent.duration._

object RetrySupervised extends App {

  def one: SupervisorStrategy = {
    OneForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 10 seconds) {
      case _: ArithmeticException => Resume
      case _: NullPointerException => Restart
      case _: IllegalArgumentException => Stop
      case _: Exception => Escalate
    }
  }

  def all: SupervisorStrategy = {
    AllForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 10 seconds) {
      case _: ArithmeticException => Resume
      case _: NullPointerException => Restart
      case _: IllegalArgumentException => Stop
      case _: Exception => Escalate
    }
  }

  val system = ActorSystem("BackoffSupervisor")
  val childProps = Props(classOf[Retry])

  val supervisor = BackoffSupervisor.props(
    Backoff.onStop(
      childProps,
      childName = "myRetry",
      minBackoff = 3.seconds,
      maxBackoff = 30.seconds,
      randomFactor = 0.2 // adds 20% "noise" to vary the intervals slightly
    ))

  val retry = system.actorOf(supervisor, name = "retrySupervisor")
  retry ! TryOperation()
}